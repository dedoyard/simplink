## SimpLink

This repository contains the source code of a very simple links page which is build with basic html and css and is easy to adjust to your needs.

The example now, contains only one link to wikipedia. In order to make more tiles just copy the single line that is between the body tags. The tiles will automatically take up the space you have on the screen so there is no need to define a width.

To adjust the content and appearance of the tile you need to change the following in each line:

- href contains the target of the link
- the middle div class can be adjusted to another color. Most of the common colors have been added to css. So to make this white we would change the class to "cell white"
- the <p> contains the text inside the tile.
- img contains the image that is added in the tile. You can experiment a bit with heights but 100px is a good height for the default tile size. You may need to adjust this if the image takes up too much space if, for example, the text is too long. If you do not want an image just delete the entire image tag.

If you want to have multiple pages. Just copy index.html to another name and use that name in the href section. There is an image called back.png in the images dir that can be used to go back a page. The href for such a button would be the name of the previous page (in this case index.html).

To create extra images you can use a variety of tools. I use gimp. Here is a tutorial on how to do that:

https://www.guidingtech.com/image-background-transparent-gimp/.

Ideally it would be saved as 100px x 100px. This is the best size for the current tile size.

An example of a possible page:

![screenshot](simpLink-example.png)
